const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Favorites = require('../models/favorite');
const authenticate = require('../authenticate');
const cors = require('./cors');
const favoriteRouter = express.Router();

favoriteRouter.use(bodyParser.json());

favoriteRouter.route('/')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus=200;})

.get(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Favorites.findOne({user:req.user._id})
    .populate('user')
    .populate('dishes')
    .then((favorites) => {
        res.statusCode=200;
        res.setHeader('Content-Type', 'application/json');
        res.json(favorites);
    }, (err) => next(err))
    .catch((err) => next(err));
})

.post(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Favorites.findOne({user:req.user._id})
    .then((favorites) => {
        if(favorites!=null)
         {
            for(var i = 0; i < req.body.length; i++)
            { dishId = req.body[i]._id;
              console.log(dishId);
                if(favorites.dishes.indexOf(dishId) == -1)
                    {
                            favorites.dishes.push(dishId);
                            favorites.save()
                            .then((favorites) => {
                                res.statusCode=200;
                                res.setHeader('Content-Type', 'application/json');
                                res.json(favorites);
                            })
                    }
                    else 
                    {
                        err = new Error('Dish '+dishId+' is already in your favorites');
                        err.status=500;
                        next(err);
                    }
                }
            
         }
         else {
            Favorites.create({})
            .then((favorites) => {
                for(var i = 0; i < req.body.length; i++)
                    {
                        dishId = req.body[i]._id;
                        favorites.dishes.push(dishId);
                    }
                favorites.user = req.user._id;
                favorites.save()
                .then((favorites) => {
                    res.statusCode=200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorites);
                })
                
            }, (err) => next(err))
            .catch((err) => next(err));
                   
            }
         })
})

.delete(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Favorites.findOneAndDelete({user: req.user._id}, (err, doc) => {
        if(!err)
            {
                res.statusCode=200;
                res.setHeader('Content-Type', 'application/json');
                res.json(doc);
            }
    })
})

favoriteRouter.route('/:dishId')
.options(cors.corsWithOptions, (req,res) => {res.sendStatus=200;})

.post(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    Favorites.findOne({user:req.user._id})
    .then((favorite) => {
        if(favorite!=null)
            {
                if(favorite.dishes.indexOf(req.params.dishId) == -1)
                {
                        favorite.dishes.push(req.params.dishId);
                        favorite.user = req.user._id;
                        favorite.save()
                        .then((favorite) => {
                            res.statusCode=200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        })
                }
                else {
                    err = new Error('Dish '+req.params.dishId+' is already in your favorites');
                    err.status=500;
                    next(err);
                }
            }
        else {
            Favorites.create(req.body)
            .then((favorite) => {
                favorite.dishes.push(req.params.dishId);
                favorite.user = req.user._id;
                favorite.save()
                .then((favorite) => {
                    res.statusCode=200;
                    res.setHeader('Content-Type', 'application/json');
                    res.json(favorite);
                })
                
            }, (err) => next(err))
            .catch((err) => next(err));
        }
    });
    
})

.delete(cors.corsWithOptions, authenticate.verifyUser, (req,res,next) => {
    
    Favorites.findOne({user:req.user._id})
    .then((favorites) => {
        if(favorites!=null )
            {
                favorites.dishes.splice(favorites.dishes.indexOf(req.params.dishId), 1);
                favorites.save()
                .then((favorites) => {
                        res.statusCode=200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(favorites);
                }, (err) => next(err))                
                
            }
            else if(favorites == null)
            {
                err = new Error('Favorites for user '+req.user._id+' not found');
                err.status=404;
                next(err);
            }
            else {
                err = new Error('Dish '+req.params.dishId+' not found');
                err.status=404;
                next(err);
            }
        
    }, (err) => next(err))
    .catch((err) => next(err));
});

module.exports = favoriteRouter;
